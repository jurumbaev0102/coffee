from django.shortcuts import render, get_object_or_404
from .models import Category, Product
from django.views.generic import RedirectView


def product_list(request, category_slug=None):
    category = None
    categories = Category.objects.all()
    products = Product.objects.filter(available=True)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    return render(request, 'product/list.html',
                  {'category': category,
                   'categories': categories,
                   'products': products})


def product_detail(request, id, slug):
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    return render(request, 'product/detail.html', {'product': product})

# class ProductLike(RedirectView):
#     def get_redirect_url(self, *args, **kwargs):
#         id_ = self.kwargs.get('pk')
#         obj = get_object_or_404(Post, pk=id_)
#         url_ = obj.get_absolute_url()
#         user = self.request.user
#         if user.is_authenticated:
#             if user in obj.likes.all():
#                 obj.likes.remove(user)
#             else:
#                 obj.likes.add(user)
#         return url_

def like_product(request):
    product = get_object_or_404(Product, id=request.PRODUCT.get('product_id'))
    post.likes.add(request.user)
    return HttpResponseRedirect(product.get_absolute_url())


# def post_comment(request, year, month, day, post):
#     product = get_object_or_404(Product, slug=post, publish__year=year, publish__month=month, publish__day=day)
#     comments = product.comments.filter()
#     new_comment = None
#     if request.method == 'PRODUCT':
#         comment_form = CommentForm(data=request.PRODUCT)
#         if comment_form.is_valid():
#             new_comment = comment_form.save(commit=False)
#             new_comment.product=product
#             new_comment.save()
#     else:
#         comment_form = CommentForm()
#     return render(request, 'detail.html', {'product':product,
#                                             'comments':comments, 'new_comment':new_comment, 'comment_form'})



